shopt -s nullglob
for f in *.m4a
do
        echo "Converting file - $f to MP3..."
        ffmpeg -i "$f" -vn -sn -c:a mp3 -ab 192k "$f.mp3"
done
