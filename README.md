# Simple Bash Scripts #

A collection of simple, reusable, bash scripts for various scenarios. No copyright is claimed over any of these scripts, and they are all free for you to use in your own workflows and or projects.

### Audio Conversion Tools ###

#### aac2mp3.sh : ACC to MP3 conversion at 192kbs ####

##### Usage #####

In a directory of .aac files to convert:
```
./aac2mp3.sh
```
Remember: aac2mp3.sh must be executable

##### Requires #####

* [ffmpeg](https://ffmpeg.org/)

#### m4a2mp3.sh : M4A to MP3 conversion at 192kbs ####

##### Usage #####

In a directory of .m4a files to convert:
```
./m4a2mp3.sh
```
Remember: m4a2mp3.sh must be executable

##### Requires #####

* [ffmpeg](https://ffmpeg.org/)
